let loggedInCredentials = localStorage.getItem("id")

if(!loggedInCredentials || loggedInCredentials == null) {
	let loginForm = document.getElementById('login');

	loginForm.addEventListener('submit', (e) => {
		e.preventDefault()

		let email = document.getElementById('email').value
		let password = document.getElementById('password').value

		if (((email != "" && password != "") || (email != "")) && (password !="")) {
				fetch('https://stark-citadel-27510.herokuapp.com/api/users/profile', {
				method : 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: email,
					password: password
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if (data.message === 'Login successfully.') {
				localStorage.setItem("id", data.result.userLogin._id)
				localStorage.setItem("firstName", data.result.userLogin.firstName)
				localStorage.setItem("lastName", data.result.userLogin.lastName)
				localStorage.setItem("email", data.result.userLogin.email)
				localStorage.setItem("password", data.result.userLogin.password)
				localStorage.setItem("isAdmin", data.result.userLogin.isAdmin)
				localStorage.setItem("mobileNumber", data.result.userLogin.mobileNumber)
				localStorage.setItem("courseId", data.result.userLogin.enrollment[0].courseId)
				localStorage.setItem("enrolledOn", data.result.userLogin.enrollment[0].enrolledOn)
				localStorage.setItem("isCurrentlyEnrolled", data.result.userLogin.enrollment[0].isCurrentlyEnrolled)
				alert(`Hello ${data.result.userLogin.firstName} ${data.result.userLogin.lastName}, you have logged in.`)

				window.location.replace('./pages/profile.html')
				} else {
					alert(data.message)
				}
			}) 	
		} else {
			alert('Invalid username or password. Please make sure to fill out both fields.')
		}
	})
} else {
	alert('You are already logged in.')
	window.location.replace('./pages/profile.html')
}