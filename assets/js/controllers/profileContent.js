let loggedInCredentials = localStorage.getItem("id")


if(!loggedInCredentials || loggedInCredentials == null){
	alert("Login first!")
	window.location.replace('../index.html')
} else {
	let firstName = localStorage.getItem('firstName')
	let lastName = localStorage.getItem('lastName')
	let isAdmin = localStorage.getItem('isAdmin')
	let mobileNumber = localStorage.getItem('mobileNumber')
	let courseId = localStorage.getItem('courseId')
	let enrolledOn = localStorage.getItem('enrolledOn')
	let isCurrentlyEnrolled = localStorage.getItem('isCurrentlyEnrolled')
	let logout = document.getElementById('logout')
	let deactivate = document.getElementById('deactivate')
	let deleteForm = document.getElementById('deleteForm')
	let password = localStorage.getItem('password')

	document.getElementById('firstName').innerHTML = firstName
	document.getElementById('lastName').innerHTML = lastName
	document.getElementById('isAdmin').innerHTML = isAdmin
	document.getElementById('mobileNumber').innerHTML = mobileNumber
	document.getElementById('courseId').innerHTML = courseId
	document.getElementById('enrolledOn').innerHTML = enrolledOn
	document.getElementById('isCurrentlyEnrolled').innerHTML = isCurrentlyEnrolled

	logout.addEventListener('submit', (e) => {
		localStorage.clear()
		alert('Logout successfully.')
	})

	deactivate.addEventListener('submit', (e) => {
		e.preventDefault()
		inputPassword = prompt('Please enter your password to deactivate your account.')
		if (password == inputPassword) {
			fetch(`https://stark-citadel-27510.herokuapp.com/api/users/deactivate/${loggedInCredentials}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json'
				}
			})
			.then(response => response.json())
			.then(data => {
				if (data.message == `User ${data.result.firstName} ${data.result.lastName} has been deactivated.`) {
					alert(data.message)
					localStorage.clear()
					window.location.replace('../index.html')
				}
			})
		}	else {
			alert('Incorrect password. Please try again.')
		}
	})

	deleteForm.addEventListener('submit', (e) => {
		e.preventDefault()
		inputPassword = prompt('Please enter your password to delete your account.')
		if (password == inputPassword) {
			fetch(`https://stark-citadel-27510.herokuapp.com/api/users/delete/${loggedInCredentials}`, {
				method: 'delete',
				headers: {
					'Content-Type': 'application/json'
				}
			})
			.then(response => response.json())
			.then(data => {
				if (data.message == `User ${data.result.firstName} ${data.result.lastName} successfully deleted.`) {
					alert(data.message)
					localStorage.clear()
					window.location.replace('../index.html')
				}
			})
		} else {
			alert('Incorrect password. Please try again.')
		}
	})
}