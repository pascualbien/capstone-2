let loggedInCredentials = localStorage.getItem("id")


if(!loggedInCredentials || loggedInCredentials == null) {
	let signup = document.getElementById('registerForm')

	signup.addEventListener('submit', (e) => {
		e.preventDefault()
		let firstName = document.getElementById('firstName').value
		let lastName = document.getElementById('lastName').value
		let email = document.getElementById('email').value
		let password = document.getElementById('password').value
		let isAdmin = document.getElementById('isAdmin').value
		let mobileNumber = document.getElementById('mobileNumber').value
		let courseId = document.getElementById('courseId').value
		let enrolledOn = document.getElementById('enrolledOn').value

		let isCurrentlyEnrolled = document.getElementById('isCurrentlyEnrolled').value
		if ((((firstName != "" && lastName != "" ) && (email != "" && password != "")) && ((isAdmin != "" && mobileNumber != "") && (courseId != "" && enrolledOn != ""))) && (isCurrentlyEnrolled != "")) {
			fetch('https://stark-citadel-27510.herokuapp.com/api/users/register', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password,
					isAdmin: isAdmin,
					mobileNumber: mobileNumber,
					courseId: courseId,
					enrolledOn: enrolledOn,
					isCurrentlyEnrolled: isCurrentlyEnrolled
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data.message == `User ${data.result.newUser.firstName} ${data.result.newUser.lastName} has been successfully registered.`) {
					alert(`User  ${data.result.newUser.firstName} ${data.result.newUser.lastName} have registered successfully.`)
					window.location.replace('../index.html')
				} else {
					alert('Something went wrong.')
				}
			})
		} else {
			alert("All fields must be filled out to register.")
		}
	})
} else {
	alert("You are already logged in!")
	window.location.replace('./profile.html')
}

