let loggedInCredentials = localStorage.getItem("id")


if(!loggedInCredentials || loggedInCredentials == null) {
	alert("Login first!")
	window.location.replace('../index.html')
} else {
	let update = document.getElementById('updateForm')

	update.addEventListener('submit', (e) => {
		e.preventDefault()
		let firstName = document.getElementById('firstName').value
		let lastName = document.getElementById('lastName').value
		let email = document.getElementById('email').value
		let password = document.getElementById('password').value
		let isAdmin = document.getElementById('isAdmin').value
		let mobileNumber = document.getElementById('mobileNumber').value
		let courseId = document.getElementById('courseId').value
		let enrolledOn = document.getElementById('enrolledOn').value
		let isCurrentlyEnrolled = document.getElementById('isCurrentlyEnrolled').value
		if ((((firstName != "" && lastName != "" ) && (email != "" && password != "")) && ((isAdmin != "" && mobileNumber != "") && (courseId != "" && enrolledOn != ""))) && (isCurrentlyEnrolled != "")) {
			fetch(`https://stark-citadel-27510.herokuapp.com/api/users/update/${loggedInCredentials}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					password: password,
					isAdmin: isAdmin,
					mobileNumber: mobileNumber,
					courseId: courseId,
					enrolledOn: enrolledOn,
					isCurrentlyEnrolled: isCurrentlyEnrolled
				})
			})
			.then(response => response.json())
			.then(data => {
				if (data.message == `User ${data.result.userUpdated.firstName} ${data.result.userUpdated.lastName} has been successfully updated to ${data.result.userUpdates.firstName} ${data.result.userUpdates.lastName}.`) {
					localStorage.clear()
					window.location.replace('../index.html')
					alert(`User ${data.result.userUpdated.firstName} ${data.result.userUpdated.lastName} has been successfully updated to ${data.result.userUpdates.firstName} ${data.result.userUpdated.lastName}.`)
					
				} else if (data.message == `User ${data.result.userUpdated.firstName} ${data.result.userUpdated.lastName} has been succesfully updated.`) {
					localStorage.clear()
					window.location.replace('../index.html')
					alert(`User ${data.result.userUpdated.firstName} ${data.result.userUpdated.lastName} has been succesfully updated.`)

				} else if (data.message == 'All fields must be filled out.') {
					alert(data.message)
				}
			})
		} else {
			alert(`All fields must be filled out to update profile`)
		}
	})

}
